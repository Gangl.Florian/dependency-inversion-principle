package at.hakwt.swp4.dip;

import java.util.ArrayList;
import java.util.List;

/**
 * A configureable implementation of a {@link BusinessProcess}. The
 * {@link #configure(List)} method must be initially called to set
 * the steps for this process.
 */
public class ConfigureableBusinessProcess implements BusinessProcess
{
    private String chaptername;
    private List<BusinessProcessStep> steps;

    public ConfigureableBusinessProcess(String chaptername)
    {
        this.steps = new ArrayList<>();
        this.chaptername = chaptername;
    }



    public ConfigureableBusinessProcess(List<BusinessProcessStep> steps) {
        this.steps = new ArrayList<>();
        configure(steps);
    }

    public void configure(List<BusinessProcessStep> services)
    {
        this.steps.addAll(services);
    }

    public void run() {
        for(BusinessProcessStep step : steps) {
            step.process();
        }
    }


}
