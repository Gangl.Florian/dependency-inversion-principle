package at.hakwt.swp4.dip.custominjector;

import at.hakwt.swp4.dip.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class DIPCustomInjectorMain {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        DependencyInjector dependencyInjector = new DependencyInjector();

        BusinessProcess customerImportProcess = new ConfigureableBusinessProcess("");
        List<BusinessProcessStep> steps = new ArrayList<>();

        steps.add(dependencyInjector.createInstanceOf(LoadFromCsvStep.class));
        steps.add(dependencyInjector.createInstanceOf(DataCanBeStoredStep.class));
        steps.add(dependencyInjector.createInstanceOf(StoreDataStep.class));

        ConfigureableBusinessProcess name = new ConfigureableBusinessProcess("------Name load------");
        System.out.println(name);

        customerImportProcess.configure(steps);
        customerImportProcess.run();





        BusinessProcess christmasPresentsProcess = new ConfigureableBusinessProcess("");
        List<BusinessProcessStep> christmasSteps = new ArrayList<>();

        christmasSteps.add(dependencyInjector.createInstanceOf(choosePresent.class));
        christmasSteps.add(dependencyInjector.createInstanceOf(BuyPresentStep.class));
        christmasSteps.add(dependencyInjector.createInstanceOf((SendPresentStep.class)));


        christmasPresentsProcess.configure(christmasSteps);
        christmasPresentsProcess.run();

        //DoTos sout auslagern Code-duplizierung
        //forken und umschreiben wie es jetzt ist

    }


}
